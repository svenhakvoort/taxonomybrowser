package nl.svenhakvoort.taxbrowser.IO;

import nl.svenhakvoort.taxbrowser.TreeStructure.Node;

import java.util.Map;
import java.util.TreeMap;

/**
 * Class for building the taxonomic tree
 *
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class TreeBuilder {

    private TreeMap<Integer, Node> nodesMap;

    /**
     * Constructor of the treebuilder, creates new map
     */
    TreeBuilder() {
        nodesMap = new TreeMap<>();
    }

    /**
     * Adds a new node to the map
     *
     * @param taxID: Taxonomic ID of the node to add
     * @param node:  A node object of the node which to add
     */
    void addNode(int taxID, Node node) {
        nodesMap.put(taxID, node);
    }

    /**
     * Couples the nodes (parent and child nodes), so the tree is fully constructed
     */
    public void buildTree() {
        for (Map.Entry<Integer, Node> entry : this.nodesMap.entrySet()) {
            Node node = entry.getValue();
            int parentID = node.getParentID();
            Node parent = nodesMap.get(parentID);
            if (parent != null) {
                parent.setChild(node);
                node.setParent(parent);
            }
        }
    }

    /**
     * Gets a node by it's id
     *
     * @param taxID: a integer specifying which node to retrieve
     * @return Node: returns the node with the specified taxID
     */
    public Node getNode(int taxID) {
        return nodesMap.get(taxID);
    }

    /**
     * Retrieves the nodesMap
     *
     * @return TreeMap: A Treemap containing the taxID as integer and the corresponding node
     */
    public TreeMap<Integer, Node> getMap() {
        return this.nodesMap;
    }
}
