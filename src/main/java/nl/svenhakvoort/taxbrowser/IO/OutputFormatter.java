package nl.svenhakvoort.taxbrowser.IO;

import nl.svenhakvoort.taxbrowser.TreeStructure.Node;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * The class which is responsible for handling output formatting
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class OutputFormatter {

    /**
    * Prints the summary of a archive in a human readable format
    * @param reader: A ReadArchive object containing all nodes in a TreeMap structure
    */
    public static void summarize(ReadArchive reader, TreeBuilder builder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        System.out.printf("%-20s %-20s %4s", "File: ", reader.getPath(), System.getProperty("line.separator"));
        System.out.printf("%-20s %-20s", "Download date: ", sdf.format(reader.getDate()));
        System.out.print(System.getProperty("line.separator"));
        System.out.printf("%-20s %-20s", "Number of nodes: ", builder.getMap().size());
    }

    /**
    * Function for printing all the information about a specific node in human readable format
     * @param builder: A ReadArchive object containing all nodes in a TreeMap structure
    */
    public static void outputID(TreeBuilder builder, String id) {
        Node node = builder.getNode(Integer.parseInt(id));
        System.out.printf("%-30s %-20s %4s", "Tax ID:", node.getTaxID(), System.getProperty("line.separator"));
        System.out.printf("%-30s %-20s %4s", "Scientific Name:", node.getScientificName(), System.getProperty("line.separator"));
        System.out.printf("%-30s %-20s %4s", "Rank:", node.getRank(), System.getProperty("line.separator"));
        System.out.printf("%-30s %-20s %4s", "Parent Tax ID:", node.getParent().getTaxID(), System.getProperty("line.separator"));
        System.out.printf("%-30s %-20s %4s", "Parent Scientific Name:", node.getParent().getScientificName(), System.getProperty("line.separator"));
        System.out.printf("%-30s %-20s %4s", "Parent Rank:", node.getParent().getRank(), System.getProperty("line.separator"));
        System.out.println("Children:");

        List<Node> children = node.getChildren();
        for (int i = 1; i <= children.size(); i++) {
            Node child = children.get(i - 1);
            System.out.printf("%4d. %-10d %-20s %4s", i, child.getTaxID(), child.getScientificName(), System.getProperty("line.separator"));
        }
    }

    /**
    * Function for printing a node list in human readable format with all info
     * @param nodes: A list containing all nodes
    */
    public static void printNodeList(List<Node> nodes) {
        System.out.println("TAX_ID;PARENT_TAX_ID;RANK;SCIENTIFIC NAME;CHILD NUMBER");
        for (Node node : nodes) {
            String[] info = {Integer.toString(node.getTaxID()), Integer.toString(node.getParentID())
                    , node.getRank(), node.getScientificName(), Integer.toString(node.getChildren().size())};
            System.out.println(String.join(";", info));
        }
    }

    /**
    * Function for printing a node list in human readable format with only essential info
     * @param nodes: A list containing all nodes
    */
    public static void printeNodeListMin(List<Node> nodes) {
        System.out.println("TAX_ID;SCIENTIFIC NAME;RANK");
        for (Node node : nodes) {
            String[] info = {Integer.toString(node.getTaxID()), node.getScientificName(), node.getRank()};
            System.out.println(String.join(";", info));
        }
    }
}
