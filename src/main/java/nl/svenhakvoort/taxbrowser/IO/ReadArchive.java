package nl.svenhakvoort.taxbrowser.IO;

import nl.svenhakvoort.taxbrowser.TreeStructure.Node;

import java.io.*;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Class for reading the archive and building the tree based on this data
 *
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class ReadArchive {
    private final String PATH;
    private TreeBuilder builder = new TreeBuilder();
    private long lastModified;

    /**
     * Constructor of the class
     * @param archivePath: A string specifying the path of the archive
     */
    public ReadArchive(String archivePath) {
        this.PATH = archivePath;
    }

    /**
     * Retrieves the contents of the archive
     * @return builder: A treebuilder object containing the taxonomic tree
     */
    public TreeBuilder getContents() {
        try {
            File zip = new File(PATH);
            // Save date
            this.lastModified = zip.lastModified();

            // Read contents of the zip and process
            ZipFile archive = new ZipFile(zip);
            readNodes(archive);
            readNames(archive);
        } catch (IOException exp) {
            System.err.println(exp.getMessage());
            System.exit(12);
        }
        return builder;
    }

    /**
     * Reads the contents of a file in the specified archive
     * @param archive: A ZipFile object containing the archive
     * @param fileName: A string specifying which file to read from the archive
     */
    private BufferedReader readFile(ZipFile archive, String fileName) {
        try {
            ZipEntry nodes = archive.getEntry(fileName);
            InputStream is = archive.getInputStream(nodes);
            return new BufferedReader(new InputStreamReader(is));
        } catch (IOException exp) {
            System.err.println(exp.getMessage());
            System.exit(2);
            return null;
        }
    }

    /**
     * Reads the names.dmp file and parses this data, adding the scientific name to the nodes
     * @param archive: A ZipFile object containing the archive
     */
    private void readNames(ZipFile archive) {
        try {
            BufferedReader br = readFile(archive, "names.dmp");

            String line;
            while ((line = br.readLine()) != null) {

                String[] splitted = line.split("\\t+\\|");
                int taxID = Integer.parseInt(splitted[0]);
                if (Objects.equals(splitted[3].trim(), "scientific name")) {
                    if (builder.getNode(taxID) != null) {
                        Node node = builder.getNode(taxID);
                        node.setScientificName(splitted[1]);
                    }
                }
            }
        } catch (IOException exp) {
            System.err.println(exp.getMessage());
        }
    }


    /**
     * Reads the contents of nodes.dmp creating all the nodes
     * @param archive: A ZipFile object containing the archive
     */
    private void readNodes(ZipFile archive) {
        try {
            BufferedReader br = readFile(archive, "nodes.dmp");

            String line;
            while ((line = br.readLine()) != null) {

                Node node = parseLine(line);
                builder.addNode(node.getTaxID(), node);
            }
        } catch (IOException exp) {
            System.err.println(exp.getMessage());
        }
    }

    /**
     * Parses the content of a line and creates a new node
     * @param line: A string containing the line to parse
     */
    private Node parseLine(String line) {
        Node node;
        String[] splitted = line.split("\\t+\\|\\t+");
        if (Objects.equals(splitted[0], "1")) {
            node = new Node(Integer.parseInt(splitted[0]), splitted[2]);
        } else {
            node = new Node(Integer.parseInt(splitted[0]), splitted[2], Integer.parseInt(splitted[1]));
        }
        return node;
    }

    /**
     * Returns the PATH of the archive
     * @return String: The path of the archive
     */
    String getPath() {
        return this.PATH;
    }

    /**
     * Gets the last modified data of the archive
     * @return long: returns date at which the archive is last modified
     */
    long getDate() {
        return this.lastModified;
    }

}
