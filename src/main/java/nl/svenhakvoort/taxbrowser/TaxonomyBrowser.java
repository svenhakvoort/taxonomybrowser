/*
 * Copyright (c) 2017 Sven Hakvoort [sven.hakvoort@gmail.com].
 * All rights reserved.
 */
package nl.svenhakvoort.taxbrowser;

import nl.svenhakvoort.taxbrowser.Cli.CliRunner;
import nl.svenhakvoort.taxbrowser.IO.OutputFormatter;
import nl.svenhakvoort.taxbrowser.IO.ReadArchive;
import nl.svenhakvoort.taxbrowser.IO.TreeBuilder;
import nl.svenhakvoort.taxbrowser.TreeStructure.Node;
import nl.svenhakvoort.taxbrowser.TreeStructure.SortingTypesNode;
import nl.svenhakvoort.taxbrowser.TreeStructure.TraverseTree;
import org.apache.commons.cli.CommandLine;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * The main class in the TaxonomyBrowser application, it runs the analysis
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class TaxonomyBrowser {

    private CliRunner cliRunner;

    /**
    * Executes the program
     * @param args: A commandline object with the arguments provided by the user
    */
    public static void main(String[] args) {
        TaxonomyBrowser browser = new TaxonomyBrowser();
        browser.start(args);
    }

    /**
    * Starts the class process with the supplied arguments
     * @param args: A commandline object with the arguments provided by the user
    */
    private void start(String[] args) {
        cliRunner = new CliRunner(args);
        CommandLine cli = cliRunner.start();
        execute(cli);
    }

    /**
     * Executes the program with the supplied arguments
     * @param cli: A commandline object with the arguments provided by the user
     */
    private void execute(CommandLine cli) {
        if (cli.hasOption("archive")) {
            ReadArchive reader = new ReadArchive(cli.getOptionValue("archive"));
            TreeBuilder builder = reader.getContents();
            builder.buildTree();
            if (cli.hasOption("summary")) {
                OutputFormatter.summarize(reader, builder);

            } else if (cli.hasOption("tax_id")) {
                OutputFormatter.outputID(builder, cli.getOptionValue("tax_id"));

            } else if (cli.hasOption("list_children") || cli.hasOption("sort")) {
                if (cli.hasOption("list_children") && cli.hasOption("sort")) {
                    // Execute the process associated with the list_children and sort arguments
                    cliListChildren(cli, builder);
                } else {
                    System.out.println("The command 'list_children' must always be used in combination with 'sort', please change this and try again");
                    cliRunner.printHelp();
                }

            } else if (cli.hasOption("list_rank")) {
                // Execute the process associated with the list_rank argument
                cliListRank(cli, builder);

            } else if (cli.hasOption("fetch_lineage")) {
                // Execute the process associated with the fetch_lineage argument
                cliFetchLineage(cli, builder);
            }
        }
    }

    /**
    * Function for controlling the process needed to track the lineage from the supplied taxID to the root
    * @param cli: A commandline object with the arguments provided by the user
     * @param builder: A TreeBuilder object containing all nodes in a TreeMap structure
    */
    private void cliFetchLineage(CommandLine cli, TreeBuilder builder) {
        // get all nodes of rank
        TraverseTree traverser = new TraverseTree(builder);
        List<Node> nodes = traverser.getLineage(Integer.parseInt(cli.getOptionValue("fetch_lineage")));
        Collections.reverse(nodes);
        if (cli.hasOption("limit")) {
            try {
                OutputFormatter.printeNodeListMin(nodes.subList(0, Integer.parseInt(cli.getOptionValue("limit"))));
            } catch (NumberFormatException e) {
                System.out.println("Error: Limit must be a number, please try again.");
                System.exit(10);
            }
        } else {
            OutputFormatter.printeNodeListMin(nodes);
        }
    }


    /**
    * Function for controlling the process needed to list all nodes with a specific rank
    * @param cli: A commandline object with the arguments provided by the user
     * @param builder: A ReadArchive object containing all nodes in a TreeMap structure
    */
    private void cliListRank(CommandLine cli, TreeBuilder builder) {
        // get all nodes of rank
        TraverseTree traverser = new TraverseTree(builder);
        List<Node> allNodes = traverser.getAllNodesWithRank(cli.getOptionValue("list_rank"));

        SortingTypesNode type = SortingTypesNode.valueOf("SCI_NAME");

        Comparator<Node> sorter = Node.getSorter(type);
        allNodes.sort(sorter);

        if (cli.hasOption("limit")) {
            try {
                int limit = Integer.parseInt(cli.getOptionValue("limit"));
                if (limit >= allNodes.size()) {
                    OutputFormatter.printNodeList(allNodes);
                } else {
                    OutputFormatter.printNodeList(allNodes.subList(0, Integer.parseInt(cli.getOptionValue("limit"))));
                }
            } catch (NumberFormatException e) {
                System.out.println("Error: Limit must be a number, please try again.");
                System.exit(10);
            }
        } else {
            OutputFormatter.printNodeList(allNodes);
        }
    }


    /**
    * Function for controlling the process needed to track the list the children of a specific node
    * @param cli: A commandline object with the arguments provided by the user
     * @param builder: A ReadArchive object containing all nodes in a TreeMap structure
    */
    private void cliListChildren(CommandLine cli, TreeBuilder builder) {
        List<Node> allChildren;
        String taxID = cli.getOptionValue("list_children");
        if (cli.hasOption("omit_subspecies")) {
            TraverseTree traverser = new TraverseTree(builder);
            allChildren = traverser.getAllChildren(Integer.parseInt(taxID), true);
        } else {
            TraverseTree traverser = new TraverseTree(builder);
            allChildren = traverser.getAllChildren(Integer.parseInt(taxID), false);
        }
        SortingTypesNode type = SortingTypesNode.valueOf(cli.getOptionValue("sort"));

        Comparator<Node> sorter = Node.getSorter(type);
        allChildren.sort(sorter);
        OutputFormatter.printNodeList(allChildren);
    }
}