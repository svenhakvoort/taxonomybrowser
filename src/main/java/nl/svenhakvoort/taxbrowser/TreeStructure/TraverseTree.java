package nl.svenhakvoort.taxbrowser.TreeStructure;

import nl.svenhakvoort.taxbrowser.IO.TreeBuilder;

import java.util.*;

/**
 * A class for traversing the treemap and retrieving specific data
 *
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class TraverseTree {

    private TreeBuilder builder;
    private List<Node> listed;

    /**
     * Constructor for the traverser
     * @param builder: A integer specifying the taxID of the node
     */
    public TraverseTree(TreeBuilder builder) {
        this.builder = builder;
    }

    /**
     * Controller for getting all children of a specific node recursively
     * @param taxID: A integer specifying the taxID of the node
     * @param omit: A string specifying the rank of the node
     * @return List<Node>: A list containing all children as Node objects
     */
    public List<Node> getAllChildren(int taxID, boolean omit) {
        listed = new ArrayList<>();
        getChildrenOfNode(taxID, omit);
        return listed;
    }

    /**
     * Recursive function for retrieving all children
     * @param taxID: A integer specifying the taxID of the node
     * @param omit: A string specifying the rank of the node
     */
    private void getChildrenOfNode(int taxID, boolean omit) {
        Node node = builder.getNode(taxID);
        if (!(omit && Objects.equals(node.getRank(), "subspecies"))) {
            listed.add(node);
        }
        List<Node> children = node.getChildren();
        for (Node child : children) {
            getChildrenOfNode(child.getTaxID(), omit);
        }
    }

    /**
     * Retrieves all nodes with a specific rank
     * @param rank: the rank of which to retrieve all nodes
     * @return List<Node>: A list containing all Node objects with the specified rank
     */
    public List<Node> getAllNodesWithRank(String rank) {
        TreeMap<Integer, Node> nodes = builder.getMap();
        List<Node> output = new ArrayList<>();
        for (Map.Entry<Integer, Node> entry : nodes.entrySet()) {
            Node node = entry.getValue();
            if (Objects.equals(node.getRank(), rank)) {
                output.add(node);
            }
        }
        return output;
    }

    /**
     * Controller for retrieving all nodes from the specified node to the root
     * @param taxID: the rank from which to travel to the root
     * @return List<Node>: A list containing all Node objects from the specified node to the root
     */
    public List<Node> getLineage(int taxID) {
        listed = new ArrayList<>();
        getParentForLineage(taxID);
        return listed;
    }

    /**
     * Recursive function for retrieving all nodes from the specified node to the root
     * @param taxID: the rank from which to travel to the root
     */
    private void getParentForLineage(int taxID) {
        Node node = builder.getNode(taxID);
        listed.add(node);
        if (taxID != 1 && node.getParentID() != 0) {
            getParentForLineage(node.getParentID());
        }
    }


}
