package nl.svenhakvoort.taxbrowser.TreeStructure;

/**
 * An enum supplying the possible sorting types
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public enum SortingTypesNode {

    TAX_ID("simple sort on taxID, numerical sort (ascending)"),
    SCI_NAME("sort on scientific name (alphabetical sort)"),
    TAX_RANK("sort on taxonomic rank  (using ordering in tree, from root to leaf)"),
    CHILD_NODES("sort on number of child nodes, numerical sort (ascending)");

    private String type;

    /**
    * Constructor which sets the sorting type
    * @param type: A string specifying the sorting type
    */
    SortingTypesNode(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SortingType{" + "type=" + type + '}';
    }
}
