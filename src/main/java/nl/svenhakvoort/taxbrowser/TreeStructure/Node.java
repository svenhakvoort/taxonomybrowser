package nl.svenhakvoort.taxbrowser.TreeStructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * A class for a Node, implements comparable for sorting of the nodes
 *
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class Node implements Comparable<Node> {

    // Ordering of ranks
    private final static String[] order = {"no rank", "superkingdom", "kingdom", "phylum", "class", "order", "superfamily", "family", "subfamily",
            "tribe", "subtribe", "genus", "subgenus", "species", "subspecies"};

    private List<Node> children = new ArrayList<>();
    private Node parent;
    private int parentID;
    private int taxID;
    private String scientificName;
    private String rank;

    /**
     * Basic constructor when only taxID and rank are known
     * @param taxID: A integer specifying the taxID of the node
     * @param rank: A string specifying the rank of the node
     */
    public Node(int taxID, String rank) {
        this.taxID = taxID;
        this.rank = rank;
    }

    /**
     * Extended constructor when only taxID and rank are known
     * @param taxID: A integer specifying the taxID of the node
     * @param rank: A string specifying the rank of the node
     * @param parentNodeID: The taxID of the parent node
     */
    public Node(int taxID, String rank, int parentNodeID) {
        this.parentID = parentNodeID;
        this.taxID = taxID;
        this.rank = rank.trim();
    }

    /**
     * provides a range of possible sorters, based on the type that is requested.
     *
     * @param type: The sorting type
     * @return nodeSorter: A comparator to sort nodes in a collection
     */
    public static Comparator<Node> getSorter(SortingTypesNode type) {
        if (type == null) {
            throw new IllegalArgumentException("Not a valid sorting type, please use a valid sorting type");
        }
        switch (type) {
            case SCI_NAME:
                return (n1, n2) -> String.CASE_INSENSITIVE_ORDER.compare(n1.scientificName, n2.scientificName);
            case TAX_RANK:
                return Comparator.comparingInt(n2 -> Arrays.asList(order).indexOf(n2.getRank()));
            case CHILD_NODES:
                return Comparator.comparingInt(n -> n.getChildren().size());
            default:
                return Comparator.comparingInt(n -> n.taxID);
        }
    }

    /**
     * overrides the hashCode function to generate a custom hash
     * @return int: A integer for hash
     */
    public int hashCode() {
        return this.taxID * this.parentID;
    }

    /**
     * overrides the equals function to create a custom comparison
     * @return bool: A boolean indicating if two objects are equal
     */
    public boolean equals(Node node) {
        return this.taxID == node.taxID && this.parentID == node.parentID;
    }


    /**
     * overrides the compareTo function to create a custom comparison
     * @return bool: A boolean indicating if two objects are equal
     */
    @Override
    public int compareTo(Node o) {
        return Integer.compare(this.taxID, o.taxID);
    }


    /* ************************************************************************
     * *                            Setters                                   *
     * ************************************************************************
     */
    public void setChild(Node node) {
        this.children.add(node);
    }

    public void setParent(Node node) {
        this.parent = node;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName.trim();
    }

    public void setChild(List<Node> nodes) {
        this.children.addAll(nodes);
    }


    /* ************************************************************************
     * *                            Getters                                   *
     * ************************************************************************
     */
    public int getTaxID() {
        return this.taxID;
    }

    public String getScientificName() {
        return this.scientificName;
    }

    public String getRank() {
        return this.rank;
    }

    public Node getParent() {
        return this.parent;
    }

    public int getParentID() {
        return this.parentID;
    }

    public List<Node> getChildren() {
        return this.children;
    }

}
