package nl.svenhakvoort.taxbrowser.Cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;


/**
 * The class for building the options, specifies which options can be provided by the user
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
class CliOptions {
    private final String HELP = "help";
    private final String ARCHIVE = "archive";
    private final String SUMMARY = "summary";
    private final String TAXID = "tax_id";
    private final String OMIT = "omit_subspecies";
    private final String LIST = "list_children";
    private final String SORT = "sort";
    private final String LISTRANK = "list_rank";
    private final String LIMIT = "limit";
    private final String FETCH = "fetch_lineage";
    private Options options = new Options();

    /**
     * The constructor and function for creating the allowed options on the command line
     */
    CliOptions() {
        // Create a option for the archive input
        Option archiveInput = Option.builder(ARCHIVE.substring(0,1)).hasArg(true)
                .desc("A zip file containing the taxonomy information").longOpt(ARCHIVE).required(true).build();
        Option summary = Option.builder(SUMMARY.substring(0, 1) + "u").hasArg(false)
                .desc("Generates a summary with basic information about the archive").longOpt(SUMMARY).required(false).build();
        Option ID = Option.builder(TAXID.substring(0, 1)).hasArg(true)
                .desc("The taxID for which to perform a task").longOpt(TAXID).required(false).build();
        Option list = Option.builder(LIST.substring(0, 1)).hasArg(true)
                .desc("The taxID of which to list the children").longOpt(LIST).required(false).build();
        Option omit = Option.builder(OMIT.substring(0, 1)).hasArg(false)
                .desc("Flag to specify if subspecies need to be omitted").longOpt(OMIT).required(false).build();
        Option sort = Option.builder(SORT.substring(0, 1)).hasArg(true)
                .desc("The sorting type, one of: TAX_ID, SCI_NAME, TAX_RANK or CHILD_NODES").longOpt(SORT).required(false).build();
        Option listRank = Option.builder(LISTRANK.substring(0, 1) + "c").hasArg(true)
                .desc("List all nodes of a specific rank").longOpt(LISTRANK).required(false).build();

        Option fetch = Option.builder(FETCH.substring(0, 1)).hasArg(true)
                .desc("The taxID of which to fetch the lineage from the root").longOpt(FETCH).required(false).build();

        Option lim = Option.builder(LIMIT.substring(0, 1) + "im").hasArg(true)
                .desc("The limit of nodes to show").longOpt(LIMIT).required(false).build();

        options.addOption(archiveInput);
        options.addOption(summary);
        options.addOption(ID);
        options.addOption(list);
        options.addOption(omit);
        options.addOption(sort);
        options.addOption(listRank);
        options.addOption(fetch);
        options.addOption(lim);


        // Create help Options
        Option helpOption = Option.builder(HELP.substring(0, 1)).hasArg(false).required(false).longOpt(HELP)
                .desc("Display usage information").build();
        options.addOption(helpOption);

    }

    /**
     * Retrieves the options variable
     * @return options: A Options object containing the information of the available options which can be supplied
     */
    Options getOptions() {
        return options;
    }

}
