package nl.svenhakvoort.taxbrowser.Cli;

import org.apache.commons.cli.CommandLine;

/**
 * The class which parses the command line arguments and verifies them
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 0.0.1
 */
public class CliRunner {
    private CliArgsProcessor processor;
    private String[] cliArgs;

    /**
     * Constructor which also calls the class Cli start()
     * @param args: List of strings which contains the command line arguments
    */
    public CliRunner(String[] args) {
        this.cliArgs = args;
        start();
    }

    /**
     * Function for starting the processing of the command line arguments
     * @return CommandLine: a commandline object with the supplied arguments
    */
    public CommandLine start() {
        processor = new CliArgsProcessor(cliArgs);
        return processor.getCli();
    }

    /**
     * Calls the help function of the CliArgsProcessor
    */
    public void printHelp() {
        processor.printHelp();
    }

}
