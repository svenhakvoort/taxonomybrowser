package nl.svenhakvoort.taxbrowser.Cli;

import nl.svenhakvoort.taxbrowser.TaxonomyBrowser;
import org.apache.commons.cli.*;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Arrays;

/**
 * Class responsible for parsing the arguments
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
class CliArgsProcessor {
    private CommandLine cli;
    private CliOptions options;
    private String[] cliArgs;

    /**
    * Constructor of the class
    * @param cli: A String array containing the command line arguments
    */
    CliArgsProcessor(String[] cli) {
        this.cliArgs = cli;
        this.options = new CliOptions();
        parseArguments();
    }

    /**
    * A function to determine the name of the running JAR, used by the printHelp() function
    * to provide a correct usage example instead of a generic one
    */
    private static String getRunningJar() {
        try {
            // Get the path and strip it down to the jar name
            File dir = new File(TaxonomyBrowser.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            String[] path = dir.getName().split("\\\\");
            return "java -jar " + path[path.length - 1];

        } catch (URISyntaxException e) {
            e.printStackTrace();
            return "Error getting running JAR";
        }
    }

    /**
    * Function for determining if the arguments can be parsed or the help needs to be called
    */
    private void parseArguments() {
        CommandLineParser parser = new DefaultParser();

        try {
            // Check if help is in the arguments, else start processing
            if (Arrays.asList(cliArgs).contains("help") || Arrays.asList(cliArgs).contains("h")) {
                printHelp();
            } else {
                this.cli = parser.parse(options.getOptions(), cliArgs);
            }
        } catch (ParseException | NullPointerException e) {
            System.err.println(e.getMessage() + "\n");
            printHelp();
            System.exit(3);
        }
    }

    /**
    * Prints a usage example in a human readable format
    */
    void printHelp() {
        String head = "\nThe following arguments can be used: \n";
        String foot = "\nIf you have any issues, please report at: https://bitbucket.org/svenhakvoort/taxonomybrowser/issues";

        HelpFormatter helpFormat = new HelpFormatter();
        System.out.println(getRunningJar());
        helpFormat.printHelp(getRunningJar(), head, options.getOptions(), foot, true);
    }

    /**
    * Gets the cli variable
    * @return cli: A String array containing the command line arguments
    */
    CommandLine getCli() {
        return this.cli;
    }
}
