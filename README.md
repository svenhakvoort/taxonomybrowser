# Taxonomy browser #

A application making the analysis and traversal of a taxonomic tree a lot easier. It provides the user the ability to filter the results as they wish.

### What is this repository for? ###

* Quick summary
* 1.0
* Sven Hakvoort, Hakvoort Development 2017

### How do I get set up? ###

* Summary of set up
  
In order to use this application you can fork this project into your own bitbucket account (You can find this in the Actions submenu of the Bitbucket repo: the three dots in the left menu left).

* Usage examples

#### Basic usage example ####:

`java -Xmx4g -jar TaxBrowser-jar-1.0.jar  -h`

#### Summary ####

`java -Xmx4g -jar TaxBrowser-jar-1.0.jar  --archive <archive> --summary`

#### Summary ####

`java -Xmx4g -jar TaxBrowser-jar-1.0.jar  --archive data/taxdmp.zip --tax_id 9606`

#### List Children and Sort ####

`java -Xmx2g -jar TaxBrowser.jar --archive data/taxdmp.zip --list_children 9604 --sort TAX_RANK `

#### List Children and Sort, omitting subspecies ####

`java -Xmx2g -jar TaxBrowser.jar --archive data/taxdmp.zip --list_children 9604 --sort TAX_RANK --omit_subspecies`

#### List all organisms with a specific rank ####

`java -Xmx2g -jar TaxBrowser.jar --archive data/taxdmp.zip --list_rank superkingdom`

#### Fetch the lineage for the specified node to the root #####

`java -Xmx2g -jar TaxBrowser.jar --archive data/taxdmp.zip --fetch_lineage 9604`

### Who do I talk to? ###

Sven Hakvoort  
Owner en Developer at Hakvoort Development    
sven@hakvoortdevelopment.nl
